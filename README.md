# EvaUnitOne
## Um robô por Guilherme Tanajura para Robocode
 
   EvaUnitOne é um robô focado em combate 1v1 (batalhas entre somente dois robôs). Ele primeiro escaneia a arena procurando por um oponente. Ao encontrar o seu oponente, ele mantém sua mira focada neste alvo, atirando rapidamente no seu inimigo, fazendo movimentos rápidos para desviar da retaliação do inimigo.


   Como dito, por ele focar em somente um inimigo de cada vez, ele funciona muito melhor em um ambiente 1v1, rapidamente desviando e atirando. Entretanto, por causa deste design, ele pode sofrer bastante em ambientes com mais de um inimigo. O seu scan da área às vezes falha se o inimigo estiver muito longe também, fazendo combate muito distante dele problemático, especialmente por sua falta de movimentação.

   
   A melhor parte foi definitivamente o processo de teste dele contra outros robôs. Fiquei mudando pequenos pedaços da estratégia dele com cada falha ou vitória, procurando a wiki e o API do Robocode para encontrar novas estratégias, e chegou a um ponto que eu mesmo estava comemorando com franqueza toda vez que ele ganhava. Aprendi muito sobre programação no java e etiqueta sobre programação fazendo essas observações, e definitivamente melhorou minha habilidade para fazer codigo.
