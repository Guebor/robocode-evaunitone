package cs;
import robocode.*;
import java.awt.Color;
/**
 * EvaUnitOne - a robot by Guilherme Tanajura
 */
public class EvaUnitOne extends AdvancedRobot {
	
		
		public void run() {
		setColors (new Color(134, 71, 194), new Color(133, 245, 88), new Color(247, 186, 44), Color.white, Color.white);
		while(true) {
			ahead(100);
			turnGunRight(360);
			back(100);
			turnGunRight(360);
		}
	}
public void onScannedRobot(ScannedRobotEvent e) {
		double graus = getHeading()-getGunHeading()+e.getBearing();
		turnGunRight(graus);
		fire(3);
		out.println("pew pew");
	}
	public void onHitByBullet(HitByBulletEvent e) {
		setBack(30);
		turnLeft(30);
		out.println("ai ai");
	}
	public void onHitWall(HitWallEvent e) {
		double bearing = e.getBearing();
    	turnRight(-bearing);
    	ahead(100);
	}
	public void onWin(WinEvent e) {
		for (int i = 0; i < 100; i++) {
			turnRight(10);
			turnLeft(10);
		}
	}	
}
